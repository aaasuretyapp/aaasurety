const express = require('express');
const tblBondRouter = express.Router();
const fatalError = 'fatal error';
const TblBondServiceClass = require('../services/TblBondService.js');
const TblBondService = new TblBondServiceClass();

tblBondRouter.post('/createTblBond', async (req, res) => {
  try {
    const tblBondDTO = req.body;
    const { code, message } = await TblBondService.create(tblBondDTO);
    if (code !== 0) {
      res.statusCode = 500;
      res.end(message || fatalError);
    }
    res.statusCode = 200;
    res.end(message);
  }
  catch (error) {
    console.log(error);
    res.statusCode = 500;
    res.end(fatalError);
  }
});

tblBondRouter.post('/updateTblBond', async (req, res) => {
  try {
    const tblBondDTO = req.body;
    const { code, message } = await TblBondService.update(tblBondDTO);
    if (code !== 0) {
      res.statusCode = 500;
      res.end(message || fatalError);
    }
    res.statusCode = 200;
    res.end(message);
  }
  catch (error) {
    console.log(error);
    res.statusCode = 500;
    res.end(fatalError);
  }
});

tblBondRouter.post('/getTblBonds', async (req, res) => {
  try {
    const { fromDate, toDate, filters } = req.body;
    const tblBondsResponse = await TblBondService.getTblBonds(filters);
    if (tblBondsResponse.code !== 0) {
      res.statusCode = 500;
      res.end(tblBondsResponse.message || fatalError);
    }
    const filteredTblBonds = JSON.parse(tblBondsResponse.message).filter(tblBond => {
      if (!tblBond.dtmDate) {
        return false;
      }
      let mustReturn = true;
      if (fromDate) {
        mustReturn = new Date(tblBond.dtmDate) >= new Date(fromDate);
      }
      if (toDate && mustReturn) {
        mustReturn = new Date(tblBond.dtmDate) <= new Date(toDate);
      }
      return mustReturn;
    });
    res.statusCode = 200;
    res.end(JSON.stringify(filteredTblBonds));
  }
  catch (error) {
    console.log(error);
    res.statusCode = 500;
    res.end(fatalError);
  }
});

tblBondRouter.post('/deleteTblBond', async (req, res) => {
  try {
    const { pkID } = req.body;
    const { code, message } = await TblBondService.delete(pkID);
    if (code !== 0) {
      res.statusCode = 500;
      res.end(message || fatalError);
    }
    res.statusCode = 200;
    res.end(message);
  }
  catch (error) {
    console.log(error);
    res.statusCode = 500;
    res.end(fatalError);
  }
});

module.exports = tblBondRouter;