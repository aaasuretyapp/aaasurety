const TblBond = require('../models/TblBond.js');
const { object2SQLQueryParams, stringifyIfString } = require('../utils/utils.js');
const CreateCloudSql = require('../modules/CloudSql.js');

class TblBondService {
	async create(tblBond) {
		try {
			const CloudSql = await CreateCloudSql();
			const newTblBond = new TblBond(tblBond);
	    const { columnsString, valuesString } = object2SQLQueryParams(newTblBond);
	    let sqlQuery = `INSERT INTO tblBondData ${columnsString} VALUES ${valuesString};`;
	    const cloudSqlResponse = await CloudSql.query(sqlQuery);
	    if (cloudSqlResponse) {
	    	return { code: 0, message: JSON.stringify(cloudSqlResponse) };
	    }
	    return { code: -1, message: 'TblBond creation failed' };
		}
		catch (e) {
			console.log(e);
			return { code: -1, message: 'TblBond creation failed' };
		}
	}

	async update(tblBond) {
		try {
			const CloudSql = await CreateCloudSql();
			const { id, data } = tblBond;
			let sqlQuery = 'UPDATE tblBondData SET';
			const dataEntries = Object.entries(data);
			if (!dataEntries.length) {
				return { code: 0, message: "Nothing to do" };
			}
			dataEntries.forEach((entry, index) => {
				const [ key, value ] = entry;
				if (value || value === 0) {
					sqlQuery += ` ${key}=${stringifyIfString(value)}`;
				}
				if (index < dataEntries.length - 1) {
					sqlQuery += ',';
				}
			});
			sqlQuery += ` WHERE pkID = ${id};`;
	    const cloudSqlResponse = await CloudSql.query(sqlQuery);
	    if (!cloudSqlResponse) {
	    	return { code: -1, message: 'TblBond update error' };
	    }
	    return { code: 0, message: JSON.stringify(cloudSqlResponse) };
	  }
	  catch (e) {
	  	console.log(e);
	  	return { code: -1, message: 'TblBond update error' };
	  }
	}

	async getTblBonds(filters) {
		try {
			const CloudSql = await CreateCloudSql();
			const filtersEntries = Object.entries(filters);
			let sqlQuery = `SELECT * FROM tblBondData`;
			if (filtersEntries.length) {
				sqlQuery += ' WHERE'
				filtersEntries.forEach((entry, index) => {
					const [ key, value ] = entry;
					sqlQuery += ` ${key} LIKE '%${value}%'`;
					if (index < filtersEntries.length - 1) {
						sqlQuery += ' AND';
					}
				});
				sqlQuery += ' ORDER BY pkID DESC;';
			} else {
				sqlQuery += ' ORDER BY pkID DESC LIMIT 100;';
			}
			console.log(sqlQuery);
	    const cloudSqlResponse = await CloudSql.query(sqlQuery);
	    if (!cloudSqlResponse) {
	    	return { code: -1, message: 'Unable to get TblBonds' };
	    }
	    return { code: 0, message: JSON.stringify(cloudSqlResponse) };
		}
		catch (e) {
			console.log(e);
			return { code: -1, message: 'Unable to get TblBonds' };
		}
	}

	async delete(tblBondId) {
		try {
			const CloudSql = await CreateCloudSql();
	    const sqlQuery = `DELETE FROM tblBondData WHERE pkID = ${tblBondId};`;
	    const cloudSqlResponse = await CloudSql.query(sqlQuery);
	    if (!cloudSqlResponse) {
	    	return { code: -1, message: 'TblBond delete error' };
	    }
	    return { code: 0, message: JSON.stringify(cloudSqlResponse) };
		}
		catch (e) {
			console.log(e);
			return { code: -1, message: 'TblBond delete error' };
		}
	}
}

module.exports = TblBondService;