const object2SQLQueryParams = object => {
	const objectEntries = Object.entries(object);
	let columnsString = '(';
	let valuesString = '(';
  objectEntries.forEach((entry, index) => {
  	const [ columnName, value ] = entry;
    // console.log(columnName);
    if (value || value === 0 || value === '') {
      columnsString += columnName;
      valuesString += stringifyIfString(value);
    }
    // console.log(objectEntries[index + 1])
    if (objectEntries[index + 1] && objectEntries[index + 1][1] !== null
      && objectEntries[index + 1][1] !== undefined) {
      columnsString += ', ';
      valuesString += ', ';
    }
  });
  columnsString += ')';
  valuesString += ')';
  console.log(columnsString);
  console.log(valuesString);
  return {
  	columnsString,
  	valuesString
  };
};

const stringifyIfString = param => {
  if (typeof param === 'string') {
    if (!param.length) {
      return "''";
    }
    return JSON.stringify(param).replace(/'/g, '').replace(/"/g, "'");
  }
  return param;
}

module.exports = {
  object2SQLQueryParams,
  stringifyIfString
};
