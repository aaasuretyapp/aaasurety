class TblBond {
	constructor(tblBond) {
		this.strSuzy = tblBond.strSuzy;
		this.dtmDate = tblBond.dtmDate;
		this.strBondType = tblBond.strBondType;
		this.strPermitType = tblBond.strPermitType;
		this.sngAmount = tblBond.sngAmount;
		this.strPrincipal = tblBond.strPrincipal;
		this.strTName = tblBond.strTName;
		this.strIndemnitor = tblBond.strIndemnitor;
		this.strTitle = tblBond.strTitle;
		this.str2Indemnitor = tblBond.str2Indemnitor;
		this.strCounty = tblBond.strCounty;
		this.strMailAddr = tblBond.strMailAddr;
		this.strMailCity = tblBond.strMailCity;
		this.strState = tblBond.strState;
		this.strZip = tblBond.strZip;
		this.strStatus = tblBond.strStatus;
		this.strReinstate = tblBond.strReinstate;
		this.strApplica = tblBond.strApplica;
		this.strBondNo = tblBond.strBondNo;
		this.strConsent = tblBond.strConsent;
		this.strCollateral = tblBond.strCollateral;
		this.strCost = tblBond.strCost;
		this.strCheckNo = tblBond.strCheckNo;
		this.strAgent = tblBond.strAgent;
		this.strComments = tblBond.strComments;
		this.strCheckRen = tblBond.strCheckRen;
		this.strRenlCost = tblBond.strRenlCost;
		this.strRenlTerm = tblBond.strRenlTerm;
		this.strInvDate = tblBond.strInvDate;
		this.strCancelDt = tblBond.strCancelDt;
		this.strUnearned = tblBond.strUnearned;
		this.sngPremium = tblBond.sngPremium;
		this.sngOther = tblBond.sngOther;
		this.sngTotalPrem = tblBond.sngTotalPrem;
		this.strPaid = tblBond.strPaid;
		this.strPermitNo = tblBond.strPermitNo;
		this.strOldBondNo = tblBond.strOldBondNo;
		this.strOverage = tblBond.strOverage;
		this.bolContacted = tblBond.bolContacted;
		this.bolAddToReport = tblBond.bolAddToReport;
	}
}

module.exports = TblBond;