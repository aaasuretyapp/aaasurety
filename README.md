# Gitlab Pipeline

In order to make the gitlab pipeline work you will need to add two variables to your project

* SERVICE_ACCOUNT --> the content of the json file you created
* PROJECT_ID --> Google Project ID

Please be aware your iam permissions for this service account will need to have these permissions:

* App Engine Deployer
* App Engine Service Admin
* Cloud Build Service Account
* Compute Viewer
* Storage Object Creator
* Storage Object Viewer
* Serverless VPC Access User

# Password generator

```
date +%s | sha256sum | base64 | head -c 32 ; echo
```
