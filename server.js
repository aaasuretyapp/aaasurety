const dotenv = require('dotenv');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

dotenv.config();

'use strict';

const app = express();

// Modules imports
const logger = require('./src/modules/Logger.js');

// Routes imports
const tblBondRoutes = require('./src/routes/tblBondRoutes.js');

// Set request config and CORS
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors());

// Routes use
app.use('/tblBonds', tblBondRoutes);

const PORT = process.env.PORT || 8080;
const server = app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});

process.on('unhandledRejection', err => {
  console.error(err);
  process.exit(1);
});

module.exports = server;
